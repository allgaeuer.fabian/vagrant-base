Base Vagrantfiles
=================

Run a Vagrantfile using Vagrant's `VAGRANT_VAGRANTFILE` environment variable:

```
$ VAGRANT_VAGRANTFILE=CentOS-7 vagrant up
```

See [https://www.vagrantup.com/docs/other/environmental-variables.html](https://www.vagrantup.com/docs/other/environmental-variables.html).
